//
// Created by kenshi on 24/09/2019.
//

#include <cocos/audio/include/SimpleAudioEngine.h>
#include <cocos2d.h>

#include "NewGame.h"
#include "../utils/Utilities.h"
#include "../Player/X.h"


USING_NS_CC;

Scene *NewGame::createScene() {
    auto scene = NewGame::createWithPhysics();
    auto layer = NewGame::create();
    layer->setPhysicsWorld(scene->getPhysicsWorld());
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    scene->addChild(layer);
    return scene;
}

bool NewGame::init() {
    if(!Scene::initWithPhysics())
        return false;

    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Sprite*sprite = Sprite::create("sprites/maps/floor.png");
    Sprite*megaman = Sprite::create("sprites/megaman x/X/stance1.png");
    PhysicsBodyParser *parser = PhysicsBodyParser::getInstance();
    parser->parseJsonFile("sprites/maps/floor.json");

    PhysicsBody *physicalBody = PhysicsBody::createBox(megaman->getBoundingBox().size);
    PhysicsBody *world = parser->bodyFromJson(sprite, "floor");

/*
    RepeatForever *repeat = RepeatForever::create(X::stance()->clone());
    Sequence *sequence = Sequence::create(X::appear()->clone(), repeat);
*/
    EventListenerKeyboard *eventListener = EventListenerKeyboard::create();

    world->setDynamic(false);
    sprite->setPosition( Vec2
                                 (visibleSize.width/2 + origin.x,
                                  visibleSize.height/2 + origin.y)
    );
    megaman->setPosition( Vec2
                                  (visibleSize.width/2 + origin.x,
                                   visibleSize.height/2 + origin.y)
    );
    megaman->addComponent(physicalBody);
    sprite->addComponent(world);
    eventListener->setEnabled(true);
    eventListener->onKeyPressed = CC_CALLBACK_2(NewGame::eventCapture, this, megaman);
    eventListener->onKeyReleased = CC_CALLBACK_2(NewGame::eventRelease, this, megaman);

//    megaman->runAction(sequence);
    addListenerGraphPriority(eventListener, this);
    this->addChild(sprite);
    this->addChild(megaman);
    return true;
}

void NewGame::eventCapture(cocos2d::EventKeyboard::KeyCode keycode, Event *event, Sprite *sprite) {
    X x;
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    RepeatForever *repeat;

    switch (keycode) {
        case EventKeyboard::KeyCode::KEY_J:
            repeat = RepeatForever::create(x.shoot()->clone());
            sprite->runAction(repeat);
            audio->playEffect("sound/megaman X6/SFX/megaman/buster small.wav");
            break;

        case EventKeyboard::KeyCode::KEY_W:
        case EventKeyboard::KeyCode::KEY_UP_ARROW:
            repeat = RepeatForever::create(x.climb()->clone());
            sprite->runAction(repeat);
            sprite->runAction(MoveBy::create(10, Vec2(0, 200)));
            audio->playEffect("");
            break;

        case EventKeyboard::KeyCode::KEY_D:
        case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
            sprite->stopAllActions();
            repeat = RepeatForever::create(x.runForward()->clone());
            sprite->runAction(repeat);
            sprite->runAction(MoveBy::create(10, Vec2(400, 0)));
            audio->playEffect("");
            break;

        case EventKeyboard::KeyCode::KEY_A:
        case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
            sprite->stopAllActions();
            repeat = RepeatForever::create(x.runForward()->clone());
            sprite->runAction(repeat);
            sprite->runAction(MoveBy::create(10, Vec2(-400, 0)));
            audio->playEffect("");
            break;

        case EventKeyboard::KeyCode::KEY_S:
        case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
            repeat = RepeatForever::create(x.shoot()->clone());
            sprite->runAction(repeat);
            audio->playEffect("");
            break;

        case EventKeyboard::KeyCode::KEY_SPACE:
            repeat = RepeatForever::create(x.jumpShoot()->clone());
            sprite->runAction(repeat);
            audio->playEffect("");
            break;

        default:    break;
    }
}

void NewGame::eventRelease(cocos2d::EventKeyboard::KeyCode keycode, cocos2d::Event *event, cocos2d::Sprite *sprite) {
    X x;
    auto repeat = RepeatForever::create(x.stance()->clone());
    sprite->stopAllActions();
    sprite->runAction(repeat);
}

void NewGame::onContact() {

}
