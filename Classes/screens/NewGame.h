//
// Created by kenshi on 24/09/2019.
//

#ifndef MYGAME_NEWGAME_H
#define MYGAME_NEWGAME_H


#include <cocos/2d/CCScene.h>

class NewGame : public cocos2d::Scene {

    cocos2d::PhysicsWorld *world;
    void setPhysicsWorld(cocos2d::PhysicsWorld *physicsWorld) { this->world = physicsWorld; }
public:
    static cocos2d::Scene*createScene();

    bool init() override;

    CREATE_FUNC(NewGame);

    void eventCapture(cocos2d::EventKeyboard::KeyCode keycode,
               cocos2d::Event*event,
               cocos2d::Sprite*sprite);
    void eventRelease(cocos2d::EventKeyboard::KeyCode keycode,
                      cocos2d::Event*event,
                      cocos2d::Sprite*sprite);
    void onContact();
};


#endif //MYGAME_NEWGAME_H
