//
// Created by kenshi on 26/09/2019.
//

#ifndef MYGAME_MENU_H
#define MYGAME_MENU_H


#include <cocos/2d/CCScene.h>

class Menu : cocos2d::Scene{

    bool init() override;

public:
    static cocos2d::Scene*createScene();
    CREATE_FUNC(Menu);
};


#endif //MYGAME_MENU_H
