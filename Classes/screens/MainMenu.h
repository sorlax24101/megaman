#ifndef __MAIN_SCENE_H__
#define __MAIN_SCENE_H__

#include "cocos2d.h"
#include <cocos/ui/UIButton.h>


class MainMenu: public cocos2d::Scene
{
    std::vector<std::string> names;
    std::vector<cocos2d::ui::Button*> buttons;
public:
	static cocos2d::Scene*createScene();
	bool init() override;

	void menuCloseCallback(cocos2d::Ref*sender);
    void newGameCallback(cocos2d::Ref*sender);
    void initKeyboard();

	CREATE_FUNC(MainMenu);
};
#endif // __MAIN_SCENE_H__
