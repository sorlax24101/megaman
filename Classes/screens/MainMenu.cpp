#include "MainMenu.h"
#include "SimpleAudioEngine.h"
#include "NewGame.h"

USING_NS_CC;

Scene*MainMenu::createScene()
{
	return MainMenu::create();
}

bool MainMenu::init()
{
	if(!Scene::init()) return false;
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
	audio->preloadBackgroundMusic("sound/mega-man-zx/02 Awake.mp3");
	audio->playBackgroundMusic("sound/mega-man-zx/02 Awake.mp3");

	auto closeItem = MenuItemImage::create(
						"CloseNormal.png",
						"CloseSelected.png",
						CC_CALLBACK_1(MainMenu::menuCloseCallback, this)
    );
	closeItem->setPosition(
				Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width / 2,
					origin.y + visibleSize.height - closeItem->getContentSize().height / 2)
    );
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 2);

	//Making label and position it on the main menu screen
	auto megamanLabel = Label::createWithTTF("Megaman", "fonts/Marker Felt.ttf", 20);
	auto nameLabel = Label::createWithTTF("Time and Space", "fonts/Marker Felt.ttf", 20);
    ui::Button* startGameButton = ui::Button::create("title screen/play button.png"),
			* quitGameButton = ui::Button::create("title screen/exit button.png");
    Sprite* sprite = Sprite::create("title screen/main menu.png");

    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

	startGameButton->setPosition(Vec2(
			visibleSize.width/4 + origin.x,
			visibleSize.height/3 + origin.y
	));
	quitGameButton->setPosition(Vec2(
			visibleSize.width/4 + origin.x,
			visibleSize.height/4 + origin.y
	));
	megamanLabel->setPosition(Vec2(
			visibleSize.width/4 + origin.x,
			origin.y*5 + visibleSize.height/2
	));
	nameLabel->setPosition(Vec2(
			visibleSize.width/4 + origin.x/2 + megamanLabel->getContentSize().width/2,
			visibleSize.height/2 + origin.y*5 - megamanLabel->getContentSize().height
	));

    quitGameButton->addClickEventListener(CC_CALLBACK_1(MainMenu::menuCloseCallback, this));
    startGameButton->addClickEventListener(CC_CALLBACK_1(MainMenu::newGameCallback, this));

	names.emplace_back("title screen/play button.png");
	names.emplace_back("title screen/exit button.png");
	names.emplace_back("title screen/chosen exit button.png");
	names.emplace_back("title screen/chosen play button.png");
	buttons.push_back(startGameButton);
	buttons.push_back(quitGameButton);

	initKeyboard();

	//adding label as a child to this layer
	this->addChild(megamanLabel, 2);
	this->addChild(nameLabel, 2);
	this->addChild(startGameButton, 2);
	this->addChild(quitGameButton, 2);
    this->addChild(sprite);

	return true;
}

void MainMenu::menuCloseCallback(cocos2d::Ref*sender)
{
	//close the game and quit the application
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    audio->playEffect("sound/megaman/03 - MenuSelect.wav");
	Director::getInstance()->end();
	#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
		exit(0);
	#endif
}

void MainMenu::newGameCallback(cocos2d::Ref *sender) {
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    audio->playEffect("sound/megaman/03 - MenuSelect.wav");
    Director::getInstance()->replaceScene(TransitionFade::create(.5, NewGame::createScene()));
    this->onExit();
}

void MainMenu::initKeyboard() {
    auto eventListener = EventListenerKeyboard::create();
	int marker = 0;

    eventListener->onKeyPressed = [&](EventKeyboard::KeyCode keycode, Event*event) {
        auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
        Vec2 loc = event->getCurrentTarget()->getPosition();

        switch (keycode) {
            case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			case EventKeyboard::KeyCode::KEY_S:
				buttons[marker]->create(names[(names.size() - 1) - marker]);
                audio->playEffect("sound/megaman zx bgm/select.wav");
				if(marker + 1 < buttons.size())
					++marker;
                break;

			case EventKeyboard::KeyCode::KEY_UP_ARROW:
			case EventKeyboard::KeyCode::KEY_W:
				if(marker - 1 > -1)
					--marker;
				audio->playEffect("sound/megaman zx bgm/select.wav");
				if(marker < names.size())
					this->buttons[marker]->create(this->names[(names.size() - 1) - marker]);
				break;

            default:
                audio->playEffect("sound/megaman zx bgm/error.wav");
                break;
        }
    };

    this->_eventDispatcher->addEventListenerWithSceneGraphPriority(eventListener, this);
}
