//
// Created by kenshi on 07/10/2019.
//

#ifndef MYGAME_CUSTOMKEYBOARD_H
#define MYGAME_CUSTOMKEYBOARD_H


#include <cocos/base/CCEventListenerKeyboard.h>
#include <unordered_set>
#include <cocos/2d/CCSprite.h>

class CustomKeyboard : cocos2d::EventListenerKeyboard{
    std::unordered_set<cocos2d::EventKeyboard::KeyCode> pressedKeys;

public:
    bool init();

    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event) { pressedKeys.insert(keyCode); }
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event) { pressedKeys.erase(keyCode); }
    void controlLogic(cocos2d::Sprite *sprite);
};


#endif //MYGAME_CUSTOMKEYBOARD_H
