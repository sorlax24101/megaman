//
// Created by kenshi on 24/09/2019.
//

#include <cocos/platform/CCFileUtils.h>
#include "Utilities.h"

PhysicsBodyParser* PhysicsBodyParser::getInstance()
{
    static PhysicsBodyParser *instance = nullptr;
    if (nullptr == instance)
        instance = new PhysicsBodyParser;
    return instance;
}

bool PhysicsBodyParser::parse(unsigned char *buffer, long length)
{
    std::string js((const char*)buffer, static_cast<unsigned long>(length));
    document.Parse<0>(js.c_str());
    return !document.HasParseError();
}

bool PhysicsBodyParser::parseJsonFile(const std::string& filename)
{
    auto content = cocos2d::FileUtils::getInstance()->getDataFromFile(filename);
    bool result = parse(content.getBytes(), content.getSize());
    return result;
}

cocos2d::PhysicsBody *PhysicsBodyParser::bodyFromJson(cocos2d::Node *node, const std::string &filename) {
    cocos2d::PhysicsBody* body = nullptr;
    rapidjson::Value &bodies = document["rigidBodies"];
    if (bodies.IsArray())
    {
        //Traverse all the bodies in the file
        for (int i = 0; i<bodies.Size(); ++i)
        {
            //Found the one requested
            if (0 == strcmp(filename.c_str(), bodies[i]["name"].GetString()))
            {
                rapidjson::Value &bd = bodies[i];
                if (bd.IsObject())
                {
                    //Create a PhysicsBody and set it according to the size of the node
                    body = cocos2d::PhysicsBody::create();
                    float width = node->getContentSize().width;
                    float offx = - node->getAnchorPoint().x*node->getContentSize().width;
                    float offy = - node->getAnchorPoint().y*node->getContentSize().height;

                    cocos2d::Point origin( bd["origin"]["x"].GetFloat(), bd["origin"]["y"].GetFloat());
                    rapidjson::Value &polygons = bd["polygons"];
                    for (int i = 0; i<polygons.Size(); ++i)
                    {
                        int pcount = polygons[i].Size();
                        auto * points = new cocos2d::Point[pcount];
                        for (int pi = 0; pi<pcount; ++pi)
                        {
                            points[pi].x = offx + width * polygons[i][pcount-1-pi]["x"].GetFloat();
                            points[pi].y = offy + width * polygons[i][pcount-1-pi]["y"].GetFloat();
                        }
                        body->addShape(cocos2d::PhysicsShapePolygon::create(points,
                                                                            pcount,
                                                                            cocos2d::PHYSICSBODY_MATERIAL_DEFAULT));
                        delete [] points;
                    }
                }
                else
                {
                    CCLOG("body: %s not found!", filename.c_str());
                }
                break;
            }
        }
    }
    return body;
}