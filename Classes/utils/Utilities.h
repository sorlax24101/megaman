//
// Created by kenshi on 24/09/2019.
//

#ifndef MYGAME_UTILITIES_H
#define MYGAME_UTILITIES_H

#include <external/json/document.h>
#include <cocos/physics/CCPhysicsBody.h>
#include "Definitions.h"

class PhysicsBodyParser {
    PhysicsBodyParser() = default;
    rapidjson::Document document;
public:
    static PhysicsBodyParser *getInstance();

    bool parseJsonFile(const std::string &filename);
    bool parse(unsigned char *buffer, long length);

    void clearCache() { document.SetNull(); }

    cocos2d::PhysicsBody *bodyFromJson(cocos2d::Node*node, const std::string &filename);
};


#endif //MYGAME_UTILITIES_H
