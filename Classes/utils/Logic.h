//
// Created by kenshi on 08/10/2019.
//

#ifndef MYGAME_LOGIC_H
#define MYGAME_LOGIC_H


#include <cocos/2d/CCActionInterval.h>
#include <cocos/2d/CCSpriteFrameCache.h>

class Logic {
public:
    static cocos2d::Animate* loadSpritesWithCache(const std::string &filePath,
                                                  const std::string &filename,
                                                  const std::string &format,
                                                  const std::string &actionName,
                                                  const int &number,
                                                  const int &startnum = 1,
                                                  float animationSpeed = .05f) {
        std::string file = filePath + filename;
        cocos2d::SpriteFrameCache::getInstance()->addSpriteFramesWithFile(file + ".plist", file + format);
        cocos2d::Animation *animation = cocos2d::Animation::create();
        for(int index = startnum; index <= number; index++) {
            std::string name = actionName;
            name.append(std::to_string(index));
            name.append(".png");
            animation->addSpriteFrame(cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(name));
        }
        animation->setDelayPerUnit(animationSpeed);
        animation->setLoops(1);
        return cocos2d::Animate::create(animation);
    }

};


#endif //MYGAME_LOGIC_H
