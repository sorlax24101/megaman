//
// Created by kenshi on 29/09/2019.
//

#ifndef MYGAME_DEFINITIONS_H
#define MYGAME_DEFINITIONS_H

#include <base/CCEventDispatcher.h>

#define dispatcher cocos2d::Director::getInstance()->getEventDispatcher()
#define addListenerGraphPriority(listener, node) dispatcher->addEventListenerWithSceneGraphPriority(listener, node)
#define addListenerWithFixedPriority(listener, node) dispatcher->addEventListenerWithFixedPriority(listener, node)
#define addCustomListener(listener, node) dispatcher->addCustomEventListener(listener, node)

#endif //MYGAME_DEFINITIONS_H
