//
// Created by kenshi on 01/10/2019.
//

#ifndef MYGAME_X_H
#define MYGAME_X_H

#include "Player.h"

class X : protected Player {
public:
    cocos2d::Animate *runForward() override;

    cocos2d::Animate *runBackward() override;

    cocos2d::Animate *jumpForward() override;

    cocos2d::Animate *dashForward() override;

    cocos2d::Animate *jumpBackward() override;

    cocos2d::Animate *dashBackward() override;

    cocos2d::Animate *shoot();

    cocos2d::Animate *saber1();
    cocos2d::Animate *saber2();
    cocos2d::Animate *saber3();
    cocos2d::Animate *saber4();

    cocos2d::Animate *jumpShoot();

    cocos2d::Animate *runShoot();

    cocos2d::Animate *uppercut();

    cocos2d::Animate *climbSlash();

    cocos2d::Animate *climb();

    static cocos2d::Animate *appear();

    static cocos2d::Animate *stance();
};


#endif //MYGAME_X_H
