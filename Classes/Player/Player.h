//
// Created by kenshi on 24/09/2019.
//

#ifndef MYGAME_PLAYER_H
#define MYGAME_PLAYER_H


#include <cocos/2d/CCSprite.h>
#include <cocos/2d/CCActionInterval.h>

class Player : cocos2d::Sprite{
public:
    virtual cocos2d::Animate* runForward() = 0;
    virtual cocos2d::Animate* jumpForward() = 0;
    virtual cocos2d::Animate* dashForward() = 0;
    virtual cocos2d::Animate* runBackward() = 0;
    virtual cocos2d::Animate* jumpBackward() = 0;
    virtual cocos2d::Animate* dashBackward() = 0;

};


#endif //MYGAME_PLAYER_H
