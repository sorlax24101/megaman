//
// Created by kenshi on 01/10/2019.
//
#include "X.h"
#include "../utils/Logic.h"

cocos2d::Animate* X::shoot() {
    const std::string path = "sprites/megaman x/X buster/",
            filename = "buster",
            format = ".png",
            actionName = "shoot";
    const int number = 2;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate* X::saber1() {
    const std::string path = "sprites/megaman x/X saber/",
            filename = "saber",
            format = ".png",
            actionName = "saber";
    const int number = 6;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate* X::saber2() {
    const std::string path = "sprites/megaman x/X saber/",
            filename = "saber",
            format = ".png",
            actionName = "saber";
    const int number = 12;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number, 7);
}

cocos2d::Animate* X::saber3() {
    const std::string path = "sprites/megaman x/X saber/",
            filename = "saber",
            format = ".png",
            actionName = "saber";
    const int number = 18;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number, 13);
}

cocos2d::Animate* X::saber4() {
    const std::string path = "sprites/megaman x/X saber/",
            filename = "saber",
            format = ".png",
            actionName = "saber";
    const int number = 24;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number, 14);
}

cocos2d::Animate *X::runForward() {
    const std::string path = "sprites/megaman x/X/",
            filename = "X",
            format = ".png",
            actionName = "run";
    const int number = 11;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate *X::runBackward() {
    const std::string path = "sprites/megaman x/X/",
            filename = "X",
            format = ".png",
            actionName = "run";
    const int number = 11;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate *X::jumpForward() {
    const std::string path = "sprites/megaman x/X/",
            filename = "X",
            format = ".png",
            actionName = "jump";
    const int number = 11;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate *X::dashForward() {
    const std::string path = "sprites/megaman x/X/",
            filename = "X",
            format = ".png",
            actionName = "dash";
    const int number = 9;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate *X::jumpBackward() {
    const std::string path = "sprites/megaman x/X/",
            filename = "X",
            format = ".png",
            actionName = "jump";
    const int number = 11;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate *X::dashBackward() {
    const std::string path = "sprites/megaman x/X/",
            filename = "X",
            format = ".png",
            actionName = "jump";
    const int number = 9;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate *X::jumpShoot() {
    const std::string path = "sprites/megaman x/X buster/",
            filename = "buster",
            format = ".png",
            actionName = "jump";
    const int number = 11;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate *X::runShoot() {
    const std::string path = "sprites/megaman x/X buster/",
            filename = "buster",
            format = ".png",
            actionName = "run";
    const int number = 11;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate *X::uppercut() {
    const std::string path = "sprites/megaman x/X saber/",
            filename = "saber",
            format = ".png",
            actionName = "saber";
    const int number = 16;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate *X::climbSlash() {
    const std::string path = "sprites/megaman x/X saber/",
            filename = "saber",
            format = ".png",
            actionName = "climb";
    const int number = 3;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number);
}

cocos2d::Animate *X::appear() {
    std::string path = "sprites/megaman x/X/",
            filename = "X",
            format = ".png",
            actionName = "transform";
    int number = 7;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number, 1, .5f);
}

cocos2d::Animate *X::stance() {
    std::string path = "sprites/megaman x/X/",
            filename = "X",
            format = ".png",
            actionName = "stance";
    int number = 5;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number, 1, 1);
}

cocos2d::Animate *X::climb() {
    std::string path = "sprites/megaman x/X/",
            filename = "X",
            format = ".png",
            actionName = "climb";
    int number = 6;
    return Logic::loadSpritesWithCache(path, filename, format, actionName, number, 1, .1f);
}

